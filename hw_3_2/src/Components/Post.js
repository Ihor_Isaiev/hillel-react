import React, {Component} from 'react';


class Post extends Component {


    render() {

        if (this.props.isLoader) {
            return (
                <div>...Loader</div>)
        }
        return (
            <div><p> {this.props.posts.map((post) => (<div key={post.id}>{post.title}</div>))}</p></div>)

    }
}

export default Post;