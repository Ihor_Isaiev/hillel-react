import React, {Component} from 'react';


class Form extends Component {
    constructor(props) {
        super(props);
        this.ref = React.createRef();
    }
    state = {
        title: '',
        body: '',
    }
componentDidMount() {
        this.ref.current.focus()
}

    handleChange = (e) => {
        let {value, name} = e.target;
        this.setState({
            [name]: value,
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (!this.state.title || !this.state.body) return;
        this.props.onSubmit(this.state);
        this.setState({
            title: '',
            body: '',
        })
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input ref={this.ref} type="text" name={'title'} value={this.state.title} onChange={this.handleChange}/>
                <input type="text" name={'body'} value={this.state.body} onChange={this.handleChange}/>
                <button type={'submit'}>Send</button>
            </form>
        );
    }
}

export default Form;