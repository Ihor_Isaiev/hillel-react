import './App.css';
import Post from "./Components/Post";
import Form from "./Components/Form";
import React, {Component} from "react";
import axios from "axios";

const url = 'https://60bb880442e1d00017620c95.mockapi.io/Posts/';

class App extends Component {

    state = {
        isError: false,
        isLoader: true,
        posts: [],
    }

    componentDidMount() {
        axios.get(url)
            .then(({data}) => {
                this.setState({
                    isLoader: false,
                    post: data,
                })
            })
    }

    onSubmit = (post) => {
        axios.post(url, post)
            .then(({data}) => {
                this.setState({
                    posts: [...this.state.posts, data]
                })
            })
            .catch((error) => {
                this.setState({
                    isError: true,
                })
            })
    };

    render() {
        return (
            <div className="App">
                <Form onSubmit={this.onSubmit}/>

                {this.state.isError ? <div>Error</div> : <Post isLoader={this.state.isLoader} posts={this.state.post}/>}
            </div>
        );
    }
}

export default App;
