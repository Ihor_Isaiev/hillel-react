import './App.css';
import Cats from "./Cat/Cats";
import Mouse from "./Mouse";

function App() {
  return (
      <div>
          <h1>Перемещайте курсор мыши!</h1>
          <Mouse render={mouse => (
              <Cats mouse={mouse}/>
          )}/>
      </div>
  );
}

export default App;
