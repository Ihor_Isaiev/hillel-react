import React from 'react';

function Categories({categories}) {
    return (
        <p key={categories.id}>{categories.map((elem) => elem ? elem.name : null)}</p>
    );
}

export default Categories;