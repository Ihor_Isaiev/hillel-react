import React from 'react';

function Data({data}) {
    function getDate(str) {
        return str.substr(0, 10);
    }
    return (
        <span>{getDate(data)}</span>
    );
}

export default Data;