import React from 'react';
import Title from "../Title/Title";
import Special from "../Special/Special";
import Autor from "../Autor/Autor";
import Content from "../Content/Content";
import Data from "../Data/Data";
import Categories from "../Categories/Categories";
import Images from "../Images/Images";


function ListItem({item}) {
    return (
        <>
            <li>
                {item.link
                    ? (<a href={item.link}>
                        <Title title={item.title} isSpecial={item.isSpecial}/>
                        {item.isSpecial ? <Special/> : null}
                        <Content content={item.content}/>
                        <Categories categories={item.categories}/>
                        <Data data={item.dateCreated}/>
                        {item.photo ? <Images images={item.photo}/> : null}
                        <Autor autor={item.author}/>
                    </a>)
                    :
                    (<>
                        <Title title={item.title} isSpecial={item.isSpecial}/>
                        {item.isSpecial ? <Special/> : null}
                        <Content content={item.content}/>
                        <Categories categories={item.categories}/>
                        <Data data={item.dateCreated}/>
                        {item.photo ? <Images images={item.photo}/> : null}
                        <Autor autor={item.author}/>
                    </>)}

            </li>
        </>
    );
}

;

export default ListItem;