import React from 'react';

function Autor({autor}) {
    return (
        <p>{autor}</p>
    );
}

export default Autor;