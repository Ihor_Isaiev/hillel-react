import React from 'react';
import List from '../List/List';
import data from '../../news';


function News(props) {
    const list = data;
    return (
        <List
        list={list}
        />
    );
}

export default News;