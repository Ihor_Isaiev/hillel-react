import React from 'react';
import ListItem from "../ListItem/ListItem";

function List({list}) {

    return (
        <ul>
            {list.map((item)=>
                <ListItem
                    key={item.id}
                    item={item}
                />)}
        </ul>
    );
}

export default List;