import React from 'react';

function PostList({isLoader, posts, onDelete, onEdit}) {


    if (isLoader) {
        return (
            <div>...Loader</div>)
    }
    return (<ul>{posts.map((post) => (
        <li key={post.id}>
            {post.body}
            <button onClick={() => onDelete(post.id)}>
                Delete
            </button>
            <button onClick={() => onEdit(post.id)}>
                Edit
            </button>
        </li>
    ))}
            </ul>)


}

;

export default PostList;