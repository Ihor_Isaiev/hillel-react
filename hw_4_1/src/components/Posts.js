import React, {useEffect, useState} from 'react';
import PostForm from "./PostForm";
import PostList from "./PostList";
import axios from "axios";

const url = 'https://60bb880442e1d00017620c95.mockapi.io/Posts/';

function Posts(props) {
    const [isLoader, setIsLoader] = useState(true);
    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(false);

    useEffect(() => {
        axios.get(url)
            .then(({data}) => {
                setPosts(data);
                setIsLoader(false)
            });

    }, [])


    let onPostSubmit = (post) => {
        axios.post(url, post)
            .then(({data}) => {
                setPosts([...posts, data]);
            })
            .catch((error) => {
                setError(true);
            });
    };

    let onEdit = (newPost) => {
        axios.put(url + newPost.id, newPost)
            .then(() => {
                setPosts([...posts, newPost])
            })
            .catch((error) => {
                setError(true)
            })
    }


    let onDelete = (id) => {
        axios.delete(url + id)
            .then(() => {
                setPosts(posts.filter((post) => post.id !== id));
            });
    };


    if (error) {
        return (
            <div>
                <PostForm onPostSubmit={onPostSubmit}/>
                <div>Error</div>

            </div>

        )
    }

    return (
        <div>
            <PostForm onPostSubmit={onPostSubmit}/>
            <PostList posts={posts} isLoader={isLoader} onDelete={onDelete}/>

        </div>
    );
}

export default Posts;