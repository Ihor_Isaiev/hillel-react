import React, {useState} from 'react';
import {useForm} from "react-hook-form";

function PostForm({onPostSubmit, onEdit}) {
    const {register, handleSubmit} = useForm();
    const [title, setTitle] =useState('');
    const [body, setBody] = useState('');
    const [newPost, setNewPost] = useState('');


    let handleEdit = (e) => {
        let {value, name} = e.target;
        if (name === 'title') {
            setTitle(value);
        }
        if (name === 'body') {
            setBody(value);
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onPostSubmit)}>

                <label> title:
                    <input
                        value={title}
                        {...register("title",
                            {
                                required: true,
                                maxLength: 20
                            })}
                        onChange={handleEdit}
                        name='title'/>
                </label>

                <label> body:
                    <input
                        value={body}
                        {...register("body",
                            {
                                required: true,
                                maxLength: 100,
                                pattern: /^[A-Za-z]+$/i
                            })}
                        onChange={handleEdit}
                        name='body'/>
                </label>

                <input type="submit"/>
            </form>
        </div>
    );
}

export default PostForm;