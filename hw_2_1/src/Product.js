import React, {Component} from 'react';
import Form from "./component/Form";


class Product extends Component {

    state = {
        productCode: '',
        title: '',
        message: 'some text',
        link: '',
        checked: {},
        typeProduct: null,
        isChecked: false,
        country: [],
    }


    handlChechShare = (e) => {
        this.setState({
            isChecked: !this.state.isChecked,
        });
    };

    handleMessage = (e) => {
        this.setState({
            message: e.target.value,
        })
    }

    handleLink = (e) => {
        this.setState({
            link: e.target.value,
        })
    }

    handleChangeCategories = (e) => {
        this.setState({
            checked: {...this.state.checked, [e.target.name]: e.target.checked},
        });
    };

    handleSelectCountry = (e) => {
        let value = e.target.value;
        this.setState({
            country: [...this.state.country, value],
        });
    };

    handleTitleProdut = (e) => {
        let validTitleCode = e.target.value;
        this.setState({
            title: validTitleCode,
        })
    }

    handleProductCode = (e) => {
        let validProductCode = e.target.value;
        validProductCode = validProductCode.replace(/\D/gmi, '');
        this.setState({

            productCode: validProductCode,
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let submitProduct = this.state;
        let server = JSON.stringify(submitProduct);
        console.log(server);
    };

    handleTypeProduct = (e) => {
        let typeProduct = e.target.value;
        this.setState({typeProduct});
    };

    render() {
        return (
            <div>
                <Form
                    submit={this.handleSubmit}
                    state={this.state}
                    handleTitleProdut={this.handleTitleProdut}
                    handleProductCode={this.handleProductCode}
                    handleChangeCategories={this.handleChangeCategories}
                    handleLink={this.handleLink}
                    handleSelectCountry={this.handleSelectCountry}
                    handleMessage={this.handleMessage}
                    handlChechShare={this.handlChechShare}
                    handleTypeProduct={this.handleTypeProduct}/>
            </div>
        );
    }
}

export default Product;

