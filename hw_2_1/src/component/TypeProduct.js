import React, {Component} from 'react';

const typeProducts = [
    'напитки',
    'конфеты',
    'печенье',
    'снеки'
];

class TypeProduct extends Component {

    render() {
        return (
            <div>
                <span>Тип продукта:</span>
                <label>

                    {typeProducts.map(el => (
                        <label key={el}> {el} <input
                            checked={this.props.typeProduct === el}
                            value={el}
                            type="radio"
                            name={el}
                            onChange={this.props.handleTypeProduct}/></label>)
                    )}
                </label>
            </div>
        );
    }
}

export default TypeProduct;