import React, {Component} from 'react';
import Select from "./Select";
import Share from "./Share";
import TypeProduct from "./TypeProduct";
import Categories from "./Categories";
import ProductCode from "./ProductCode";
import TitleProduct from "./TitleProduct";
import MainText from "./MainText";
import LinkImage from "./LinkImage";


class Form extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = props.submit;
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit} className={'form'}>
                    <ProductCode
                        productCode={this.props.state.productCode}
                        handleProductCode={this.props.handleProductCode}/>

                    <TitleProduct
                        handleTitleProdut={this.props.handleTitleProdut}
                        title={this.props.state.title}/>

                    <MainText message={this.props.state.message}
                              handleMessage={this.props.handleMessage}/>

                    <LinkImage
                        link={this.props.state.link}
                        handleLink={this.props.handleLink}/>

                    <Categories
                        checked={this.props.state.checked}
                        handleChangeCategories={this.props.handleChangeCategories}/>

                    <TypeProduct typeProduct={this.props.state.typeProduct}
                                 handleTypeProduct={this.props.handleTypeProduct}/>

                    <Share isChecked={this.props.state.isChecked}
                           handlChechShare={this.props.handlChechShare}/>

                    <Select country={this.props.state.country}
                            handleSelectCountry={this.props.handleSelectCountry}/>

                    <input type="submit" value="Отправить"/>
                </form>
            </div>
        );
    }
}

export default Form;


