import React, {Component} from 'react';

class TitleProduct extends Component {

    render() {
        return (
            <div>
                <label>
                    Заголовок: <input
                    type="text"
                    name={'title'}
                    value={this.props.title}
                    onChange={this.props.handleTitleProdut}/>
                </label>
            </div>
        );
    }
}

export default TitleProduct;