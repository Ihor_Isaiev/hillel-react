import React, {Component} from 'react';
import {findAllByDisplayValue} from "@testing-library/react";

const categories = ['элит', 'стандарт', 'дешевые']

class Categories extends Component {


    render() {
        return (
            <div>
                <span>Выбранные категории: </span>
                {categories.map(el => (
                    <div key={el}>
                        <label>{el}<input type="checkbox"
                                          name={el}
                                          checked={this.props.checked[el]}
                                          onChange={this.props.handleChangeCategories}/>
                        </label>
                    </div>
                ))}
            </div>
        );
    }
}

export default Categories;