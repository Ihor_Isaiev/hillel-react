import React, {Component} from 'react';
class ProductCode extends Component {


    render() {
        return (
            <div>
                <label>
                    Код продукта:
                    <input required type="text"
                           name={'product'}
                           value={this.props.productCode}
                           onChange={this.props.handleProductCode}/>
                </label>
            </div>
        );
    }
}

export default ProductCode;

