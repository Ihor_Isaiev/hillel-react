import React, {Component} from 'react';


class Share extends Component {

    render() {
        return (
            <label>
                Акционный продукт:
                <input
                    type="checkbox"
                    name={'share'}
                    checked={this.props.isChecked}
                    onChange={this.props.handlChechShare}/>
            </label>
        );
    }
}

export default Share;