import React, {Component} from 'react';

class MainText extends Component {

    render() {
        return (
            <div>
                <label>
                    Описание: <textarea
                    value={this.props.message}
                    onChange={this.props.handleMessage}/>
                </label>
            </div>
        );
    }
}

export default MainText;