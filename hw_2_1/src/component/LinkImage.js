import React, {Component} from 'react';

class LinkImage extends Component {

    render() {
        return (
            <div>
                <label>
                    Ссылка на изображение:
                    <input type="text"
                           name={'image'}
                           value={this.props.link}
                           onChange={this.props.handleLink}/>
                </label>
            </div>
        );
    }
}

export default LinkImage;