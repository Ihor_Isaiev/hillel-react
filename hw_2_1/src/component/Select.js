import React, {Component} from 'react';


const countries = [
    'Чехия',
    'Болгария',
    'Израиль',
    'Италия',
    'Польша'
];

class Select extends Component {

    render() {
        return (
            <div>
                <label>Страны-производители:
                    <select multiple
                            value={this.props.country}
                            onChange={this.props.handleSelectCountry}>
                        {countries.map(countryLabel => (
                            <option
                                key={countryLabel}
                                value={countryLabel}>{countryLabel}</option>
                        ))}
                    </select>
                </label>
            </div>
        );
    }
}

export default Select;