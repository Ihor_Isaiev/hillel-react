import './App.css';
import data from './news.json';
import {Fragment} from "react";




function App() {

    let arr = [];
    for(let i = 0; i <= 100; i++) {
        let info = data;
        arr = [...arr, info];
    }


    return (
        <ul>

            {arr.map(elem => (elem.map(el=> (
                    <li key={el.id} className= 'border'>
                        {el.isSpecial ? <p className='special'>HOT NEWS</p> : null}
                        {el.link ? <a href={el.link}>
                            <p>{el.title}</p>
                            <div dangerouslySetInnerHTML={{ __html: el.content }}/>

                            {el.categories.map(e => (<p>{e.name}</p>))}
                            {el.photo ? <img src={el.photo} className='img'/> : null}
                            <p>{el.author}</p>
                        </a> : <Fragment>
                            <p>{el.title}</p>
                            <div dangerouslySetInnerHTML={{ __html: el.content }}/>

                            {el.categories ? el.categories.map(e => (<p>{e.name}</p>)) : null}
                            {el.photo ? <img src={el.photo} className='img'/> : null}
                            <p>{el.author}</p>
                        </Fragment>}

                    </li>


                )))



            )}
        </ul>
    );


}
export default App;
