import React from 'react';
import PropTypes from 'prop-types';
import {ConnectedRouter} from "connected-react-router";

import {routes} from "./routes";
import {Page404} from "./shared/components/Page404";
import Container from '@material-ui/core/Container';
import Header from "./shared/components/Header/Header";
import {Route} from 'react-router';
import {Switch} from "react-router-dom";


export function AppContainer(props) {
    const {history} = props;

    return (
        <ConnectedRouter history={history}>
            <Header/>
            <Container>

                <Switch>
                    {routes.map(route => (
                        <Route key={route.key} path={route.path} exact={route.exact} component={route.component}/>
                    ))}
                    <Route path="*" exact component={Page404}/>
                </Switch>

            </Container>
        </ConnectedRouter>
    );
}

AppContainer.propTypes = {
    history: PropTypes.object,
};
