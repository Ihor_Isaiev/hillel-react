import React from 'react';


export function Page404() {
  return (
    <div className="page404">
      Page not found.
    </div>
  );
}

Page404.propTypes = {};
