import React, {Fragment} from 'react';
import {AppBar, Badge, Box, Button, IconButton, makeStyles, Toolbar} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {NavLink} from 'react-router-dom';
import SpaIcon from '@material-ui/icons/Spa';
import {useSelector} from "react-redux";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import {withStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    offset: theme.mixins.toolbar,
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));


const StyledBadge = withStyles((theme) => ({
    badge: {
        right: -3,
        top: 13,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}))(Badge);

function Header() {
    const classes = useStyles();
    const { store: { data } } = useSelector((s) => s);
    return (
        <Fragment>
            <div className={classes.offset}></div>
            <AppBar position="fixed">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <SpaIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        SpaShop
                    </Typography>
                    <Box>
                        <Button color="inherit" to='/' exact component={NavLink}>About Us</Button>
                        <Button color="inherit" to='/catalog' exact component={NavLink}>Catalog</Button>
                        <Button color="inherit" to='/delivery' exact component={NavLink}>Delivery</Button>
                        <Button color="inherit" to="/basket" exact component={NavLink}>Basket
                            {data.length<1 ? null : <IconButton aria-label="cart">
                                <StyledBadge badgeContent={data.length} color="secondary">
                                    <ShoppingCartIcon/>
                                </StyledBadge>
                            </IconButton>}
                        </Button>
                    </Box>
                </Toolbar>
            </AppBar>
        </Fragment>

    );
}

export default Header;