import {routes as basket} from "./features/basket/routes";
import {routes as aboutUs} from "./features/aboutUs/routes";
import {routes as catalog} from "./features/catalog/routes";
import {routes as delivery} from "./features/delivery/routes";

export const routes = [
    // put here features' routes
    ...catalog,
    ...aboutUs,
    ...delivery,
    ...basket,
];
