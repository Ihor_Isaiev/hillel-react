import React, {useEffect, useState} from 'react';
import Rating from '@material-ui/lab/Rating';
import {useQuery} from "react-query";
import {getList} from "../api/PostsApi";
import { Link as RouterLink } from 'react-router-dom';
import {
  Button,
  Typography,
  Badge,
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Paper,
  CardActions,
  makeStyles
} from "@material-ui/core";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Filter from "./Filter";
import debounce from "debounce"
import Categories from "./Categories";
import {useDispatch} from "react-redux";
import {addCardToBasket} from "../../basket/action";

function PostPages() {
    const useStyles = makeStyles((theme) => ({
      page:{
        display: 'flex',
      },
      post: {
        margin: 10,
      },
      root: {
        margin: 10,
      },
      submit: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: '10px',
      },
  }));

    const classes = useStyles();
    const {data, error, isLoading} = useQuery('posts', async () => {
        let {data} = await getList();
        return data;
    });


    const [filters, setFilter] = useState('');
    const [searchFilters, setSearchFilters] = useState('');
    const [cashValue, setCashValue] = useState([0, 1000]);
    const [ratingValue, setRatingValue] = useState([0, 100]);
    const [isNew, setIsNew] = useState(false);
    const [isAction, setIsAction] = useState(false);
    const [isSale, setIsSale] = useState(false);
    const [cats, setCats] = useState([]);


    const setSearchFiltersDebounce = debounce(setSearchFilters, 600);

    useEffect(() => {
        setSearchFiltersDebounce(filters)
    }, [filters])


    const filterItem = () => {
        return data.filter((item) => {
            let result = [];

            if (isNew) {
                result = result && item.isNew;
            }

            if (isSale) {
                result = result && item.isSale;
            }

            if (isAction) {
                result = result && item.isInStock;
            }

            if (searchFilters.length) {
                result = result && (item.title.toLowerCase().includes(searchFilters.toLowerCase()));
            }

            if (cashValue) {
                let [min, max] = cashValue;
                result = result && (item.price >= min && item.price <= max);
            }

            if (ratingValue) {
                let [min, max] = ratingValue;
                result = result && (item.rating >= min && item.rating <= max);
            }

            if (cats.length > 0) {
                let arr = item.categories.filter((elem)=>cats.includes(elem))
                if(arr.length < 1){
                    result = false
                }
            }
            return result;
        });
    };

    const dispatch = useDispatch();

    function addToBasket(item) {
        dispatch(addCardToBasket(item))
    }


    return (
        <Box>
            {isLoading
                ? (<Box>Loading...</Box>)
                : (error

                    ? (<Box>Error: {error.message}</Box>)
                    : (
                        <Box className={classes.page}>
                          <Box>
                            <Filter
                                setFilter={setFilter}
                                filter={filters}
                                cashValue={cashValue}
                                setCashValue={setCashValue}
                                ratingValue={ratingValue}
                                setRatingValue={setRatingValue}
                                isNew={isNew}
                                setIsNew={setIsNew}
                                isAction={isAction}
                                setIsAction={setIsAction}
                                isSale={isSale}
                                setIsSale={setIsSale}
                            />

                                <Categories selected={cats} setSelected={setCats}/>
                            </Box>

                            <Box>
                                <Grid container item xs={12} spacing={3}>
                                    {filterItem().map((item) => (
                                        <Grid key={item.id} item xs={4}>
                                            <Card className={classes.post}>
                                                <CardActionArea>
                                                    <CardMedia component="img" alt="photo" height="140" image={item.photo} title="photo product" />
                                                    <CardContent>

                                                        <CardContent>
                                                            {item.isNew
                                                              ?<Badge color="secondary" badgeContent={"NEW"}>
                                                                    <Typography variant="h6">{item.title}</Typography>
                                                               </Badge>
                                                              :<Typography variant="h6">{item.title}</Typography>}

                                                            <Typography variant={'body2'}>{item.description}</Typography>

                                                            <CardActions>
                                                              <Button size="small" component={RouterLink} to={`/posts/${item.id}`}>
                                                                Больше деталей
                                                              </Button>
                                                            </CardActions>

                                                            {item.isSale
                                                              ?<Badge color="secondary" badgeContent={"Sale"}>
                                                                   <Typography variant="h6">{item.price} $</Typography>
                                                               </Badge>
                                                              :<Typography variant="h6">{item.price} $</Typography>
                                                            }

                                                            <CardContent>
                                                                <Rating name="half-rating" defaultValue={(item.rating) / 20} precision={0.5}/>
                                                            </CardContent>

                                                            <CardActions>
                                                              <Button onClick={() => {addToBasket(item)}} className={classes.submit}>
                                                                  Добавить
                                                                  <AddShoppingCartIcon/>
                                                              </Button>
                                                            </CardActions>

                                                        </CardContent>
                                                    </CardContent>
                                                </CardActionArea>
                                            </Card>
                                        </Grid>
                                    ))}
                                </Grid>
                            </Box>

                        </Box>))}
        </Box>
    );
}

export default PostPages;
