import React, { useEffect } from 'react';
import { Box, FormControlLabel, Checkbox, makeStyles } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import {  sagaAddCategories } from "../../basket/action";

const useStyles = makeStyles({
    categoriesStyle: {
        display: 'flex',
        flexDirection: 'column',
        margin: '10px',
        backgroundColor: 'white',
        width: '240px',
        padding: '20px'
    }
});

function Categories({ selected, setSelected }) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const  list  = useSelector((s) => s.categories.list);

    useEffect(() => {
        dispatch(sagaAddCategories());
    }, []);

    let handleChange = (id) => {
        let index = selected.indexOf(id);
        let newSelected = [];
        if (index >= 0) {
            newSelected = [...selected];
            newSelected.splice(index, 1)
        } else {
            newSelected = [...selected, id];
        }
        setSelected(newSelected);

    }
    return (
        <Box className={classes.categoriesStyle}>
            {list.map((item) =>
                (<FormControlLabel
                  value="start"
                  key={item.id}
                  control={
                      <Checkbox
                      checked={selected.includes(item.id)}
                      onChange={() => handleChange(item.id)}
                      name ={item.name}
                      />
                          }
                          label={item.name}
                          labelPlacement="end"
                    />))}
        </Box>
    );
};

export default Categories;
