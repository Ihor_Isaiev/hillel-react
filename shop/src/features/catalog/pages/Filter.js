import React from 'react';

import TextField from '@material-ui/core/TextField';
import {
    Box,
    makeStyles,
    Slider,
    Switch,
    FormControl,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root:{
        margin: '10px',
        backgroundColor: 'white',
        width: '240px',
        padding: '20px'
    },
    searchFilter: {
        marginBottom: '20px',
    },
    cashFilter:{
        width: '200px',
    }
}));

function valuetext(value) {
    return `${value}`;
}

function Filter({filters, setFilter, setCashValue, cashValue,setRatingValue,ratingValue,isNew, setIsNew, isAction, setIsAction, isSale, setIsSale}) {
    const classes = useStyles();

    const onChange = (e) => {
        setFilter(e.target.value);
    }

    const handleChange = (event, newValue) => {
        setCashValue(newValue)

    }

    const handleChangeRating = (event, newValue) => {
        setRatingValue(newValue)
    }



    return (
        <Box>
            <FormControl className={classes.root} noValidate autoComplete="on">
                <TextField
                  className={classes.searchFilter}
                  id="outlined-basic"
                  label="Search by name"
                  variant="outlined"
                  value={filters}
                  onChange={onChange}
                />

                <Box className={classes.cashFilter}>
                    <Typography id="range-slider" gutterBottom>
                        Filter by price
                    </Typography>
                    <Slider
                        value={cashValue}
                        onChange={handleChange}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        getAriaValueText={valuetext}
                        min={0}
                        max={1000}
                    />
                </Box>
                <Box className={classes.cashFilter}>
                    <Typography id="range-slider" gutterBottom>
                        Filter by rating
                    </Typography>
                    <Slider
                        value={ratingValue}
                        onChange={handleChangeRating}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        getAriaValueText={valuetext}
                        min={0}
                        max={100}
                    />
                </Box>

                <Typography gutterBottom>
                    Фильтр для новинок:
                    <Switch
                        checked={isNew}
                        onChange={()=>setIsNew(!isNew)}
                        name="isNew"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </Typography>

                <Typography gutterBottom>
                    Фильтр "в наличии":
                    <Switch
                        checked={isAction}
                        onChange={()=>setIsAction(!isAction)}
                        name="isAction"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </Typography>

                <Typography gutterBottom>
                    Фильтр для распродажи:
                    <Switch
                        checked={isSale}
                        onChange={()=>setIsSale(!isSale)}
                        name="isSale"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </Typography>

            </FormControl>
        </Box>
    );
}

export default Filter;
