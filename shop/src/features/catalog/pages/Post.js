import React from 'react';
import {useParams} from "react-router";
import {useQuery} from "react-query";
import {get} from "../api/PostsApi";
import {Box, Card, CardActionArea, CardContent, CardMedia, Button, makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { Link as RouterLink } from 'react-router-dom';
import Rating from "@material-ui/lab/Rating";


const useStyles = makeStyles({
    container:{
      display:'flex',
      justifyContent:'center',
      margin:'10px',
    },
    post: {
        padding: 10,
        margin: 10,
        width: '80%'
    },
    submit: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 10px',
        float: 'right',
        margin: '20px 60px',
        width: '160px'
    },
});



export function Post() {
    const {id} = useParams();
    const classes = useStyles();
    const {data, error, isLoading} = useQuery(["posts", id], async () => {
        let {data} = await get(id);
        return data;
    });

    return (
      <Box className={classes.container}>
        <Card className={classes.post}>
            <CardActionArea>
                    {isLoading
                        ? (<Typography variant={'h4'}>Loading...</Typography>)
                        : error
                            ? (<Typography variant={'h5'}>Error: {error.message}</Typography>)
                            : (
                                <>
                                    <CardMedia
                                        component="img"
                                        alt="photo"
                                        image={data.photo}
                                        title="photo product"
                                    />
                                    <CardContent>

                                        <Box key={data.id}>

                                            <Typography variant={'h4'}>{data.title}</Typography>
                                            <Typography variant={'body2'}>{data.description}</Typography>
                                            <Box>
                                                <Typography variant={'h6'}>{data.price}$</Typography>
                                            </Box>
                                            <Box>
                                                <Rating name="half-rating"
                                                        defaultValue={(data.rating) / 20}
                                                        precision={0.5}/>
                                            </Box>
                                            <Box>
                                            <Button
                                              className={classes.submit}
                                              component={RouterLink}
                                              to={'/catalog/'}>
                                              Назад в каталог
                                            </Button>
                                            </Box>

                                        </Box>
                                    </CardContent>
                                </>
                            )}


            </CardActionArea>
        </Card>
</Box>
    );
}

Post.propTypes = {};
