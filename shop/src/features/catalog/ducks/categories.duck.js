import { all, call, spawn, takeEvery, put } from "redux-saga/effects"
import { ADD_CATEGORIES, AddCategories, SAGA_ADD_CATEGORIES } from "../../basket/action";


const initial = {
    list: [],
    error: null,

}

export function reducerCategory(state = initial, { type, payload }) {
    switch (type) {

        case ADD_CATEGORIES:
            return {
                ...state,
                list: [
                    ...payload
                ]
            }

        default:
            return state;
    }
}

function* getCategories() {
    let items = [];
    let error = null;

    try {
        let response = yield call(fetch, 'https://react-js-course-05-05-2021-bkd.herokuapp.com/category');
        items = yield call(response.json.bind(response));
        // items = yield call([response, response.json]);
        // console.log(items);
        yield put(AddCategories(items));
    } catch (err) {
        error = err.message;
        console.log(error)
    }

}

function* sagaWatcherCategories() {
    yield takeEvery(SAGA_ADD_CATEGORIES, getCategories);
}

export function* sagas() {
    const sagas = [sagaWatcherCategories];

    const retrySagas = sagas.map((saga) => spawn(function* () {
        while (true) {
            try {
                yield call(saga);
                break;
            } catch (error) {
                console.log(error);
            }
        }
    }));

    yield all(retrySagas);
}