import PostPages from './pages/PostPages';
import { featureConf } from "./config";
import {Post} from "./pages/Post";


export const routes = [
  {
    key: `${featureConf}/catalog`,
    path: '/catalog',
    component: PostPages,
    exact: true,
  },
  {
    key: `${featureConf}/posts`,
    path: '/posts/:id',
    component: Post,
    exact: true,
  },
];
