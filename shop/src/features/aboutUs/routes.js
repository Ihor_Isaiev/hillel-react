import HomePages from './pages/HomePages';
import { featureConf } from "./config";

export const routes = [
  {
    key: `${featureConf}/about`,
    path: '/',
    component: HomePages,
    exact: true,
  },
];
