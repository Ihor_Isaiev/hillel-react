import  Delivery  from './pages/Delivery';
import { featureConf } from "./config";

export const routes = [
  {
    key: `${featureConf}/delivery`,
    path: '/delivery',
    component: Delivery,
    exact: true,
  },
];
