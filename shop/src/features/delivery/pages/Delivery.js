import React from 'react';
import {Box, Typography, makeStyles} from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
    homePage: {
      display:'flex',
      alignItems: 'center',
        padding: '20px',
        margin: '20px',
    },
    homePageItem:{
      padding: '20px',
      margin: '20px',
    }
}));

function Delivery() {
  const classes = useStyles();
    return (
      <>
      <Box className={classes.homePage}>
      <Box className={classes.homePageItem}>
        <Typography variant={'h5'}>Lorem ipsum dolor sit amet</Typography>
        <Typography variant={'body2'}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Typography>
      </Box>

      <Box className={classes.homePageItem}>
      <Typography variant={'h5'}>Lorem ipsum dolor sit amet</Typography>
        <Typography variant={'body2'}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Typography>
      </Box>

      <Box className={classes.homePageItem}>
      <Typography variant={'h5'}>Lorem ipsum dolor sit amet</Typography>
        <Typography variant={'body2'}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Typography>
      </Box>
      </Box>

      </>
    );
}

export default Delivery;
