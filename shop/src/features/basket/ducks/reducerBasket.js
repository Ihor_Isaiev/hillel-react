import { all } from "redux-saga/effects"

import { CARDS_ADD_CARD, CARDS_REMOVE_CARD, ORDER_ADD_CARD } from "../action";



const init = {
    data: [],
    orders: {},
}

export function reducerBasket(state = init, { type, payload }) {
    switch (type) {
        case CARDS_ADD_CARD:
            return {
                ...state,
                data: [
                    ...state.data,
                    payload
                ]}
        case CARDS_REMOVE_CARD:
            return {
                ...state,
                data: [...state.data.filter((card) => card.id !== payload)]
            }
        case ORDER_ADD_CARD:
            return {
                ...state,
                orders: {
                    ...state.orders,
                    ...payload
                }}

        default:
            return state;
    }
}


export const sagas = function* () {
    yield all([
        // put your sagas here
    ]);
};
