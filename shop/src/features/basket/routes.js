import  Basket  from './pages/Basket';
import { featureConf } from "./config";
import OrderPage from "./pages/OrderPage";
import OrderPagesStepTwo from "./pages/OrderPagesStepTwo";
import OrderPagesStepthree from "./pages/OrderPagesStepthree";

export const routes = [
  {
    key: `${featureConf}/basket`,
    path: '/basket',
    component: Basket,
    exact: true,
  },
  {
    key: `${featureConf}/orders`,
    path: '/orders',
    component: OrderPage,
    exact: true,
  },
  {
    key: `${featureConf}/orders`,
    path: '/step2',
    component: OrderPagesStepTwo,
    exact: true,
  },
  {
    key: `${featureConf}/orders`,
    path: '/step3',
    component: OrderPagesStepthree,
    exact: true,
  },
];
