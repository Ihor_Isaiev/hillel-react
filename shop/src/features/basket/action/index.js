export const CARDS_REMOVE_CARD = 'CARDS_REMOVE_CARD';
export const CARDS_ADD_CARD = 'CARDS_ADD_CARD';
export const ORDER_ADD_CARD = 'ORDER_ADD_CARD';
export const ADD_CATEGORIES = 'ADD_CATEGORIES';
export const SAGA_ADD_CATEGORIES = 'SAGA_ADD_CATEGORIES';

export const addCardToBasket = (item) => {
    return {
        type: CARDS_ADD_CARD,
        payload: item
    }
}

export const removeCardFromBasket = (id) => {
    return {
        type: CARDS_REMOVE_CARD,
        payload: id
    }
}

export const OrderFromBasket = (state) => {
    return {
        type: ORDER_ADD_CARD,
        payload: state
    }
}

export const sagaAddCategories = () => ({type: SAGA_ADD_CATEGORIES});


export const AddCategories = (categories) => {
    return {
        type: ADD_CATEGORIES,
        payload: categories
    }
}
