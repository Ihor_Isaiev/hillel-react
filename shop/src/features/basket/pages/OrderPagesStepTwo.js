import React from 'react';
import {useSelector} from "react-redux";
import {makeStyles, Button, Box, Typography} from "@material-ui/core";
import { Link as RouterLink } from 'react-router-dom';
import ProductPosts from "./ProductPosts";
import axios from "../../../services/axios";



const useStyles = makeStyles({
  submit: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 10px',
      float: 'right',
      margin: '0 60px',
      width: '160px'
  },
  groupSubmit:{
    display: 'flex',
    justifyContent: 'center'
  },
  userData: {
    backgroundColor: 'white',
    padding: '10px',
    margin: '20px',
  }
});

function OrderPageStepTwo() {

  const classes = useStyles();
    const {store: {orders}} = useSelector((s) => s);
    const {store: {data}} = useSelector((s) => s);

    let cost = (data.length<1 ? 0 : data.map((item)=> item.price).reduce((a,b)=> +a+(+b)))



    const handleAxios = ()=>{
        axios.post('https://60e4720a5bcbca001749e9e1.mockapi.io/order',
            JSON.stringify(orders)
        )
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }




    return (
        <>
            <ProductPosts/>


            <Box>
              <Box className={classes.userData}>
                  <Box>
                    <Typography variant={'h5'}>Данные для оформления заказа:</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Имя: {orders.firstName}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Фамилия: {orders.lastName}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Страна: {orders.country}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Город: {orders.city}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Адрес: {orders.address}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Доп. адрес: {orders.address2}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Телефон: {orders.phone}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Email: {orders.email}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Способ доставки: {orders.deliveryType}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Нужен звонок: {orders.dontCallMe? 'Да': 'Нет'}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body2'}>Комментарий к заказу: {orders.comment}</Typography>
                  </Box>
                  <Box>
                    <Typography variant={'body1'}>Общая стоимость заказа: {cost} $ </Typography>
                  </Box>
              </Box>
                <Box className={classes.groupSubmit}>

                  <Button
                    className={classes.submit}
                    component={RouterLink}
                    to={'/orders/'}>
                    Вернуться назад
                  </Button>

                  <Button
                    className={classes.submit}
                    component={RouterLink}
                    to={'/step3/'}>
                    Подтвердить
                  </Button>

                </Box>
            </Box>

        </>
    );
}

export default OrderPageStepTwo;
