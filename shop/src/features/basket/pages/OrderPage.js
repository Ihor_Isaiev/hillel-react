import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
  makeStyles,
  Button,
  FormGroup,
  TextField,
  Select,
  MenuItem,
  Checkbox,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Typography,
  Box,
  InputLabel
} from "@material-ui/core";
import { Link as RouterLink } from 'react-router-dom';
import {OrderFromBasket} from "../action";


const useStyles = makeStyles({
    groupSubmit:{
      display: 'flex',
      justifyContent: 'center'
    },
    submit: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: '48px',
        padding: '0 30px',
        margin: '20px',
        width: '140px'

    },
    margin:{
      display: 'flex',
    },
    formControl: {
      margin: '10px',
      minWidth: '180px',
},
    input:{
      margin: '10px',
      width: '480px',
    }
});
const country = ['Ukraine', 'Poland', 'Israel', 'Germany', 'Austria', 'Turkey', 'Russia', 'China'];
const deliveryType = ['Самовывоз', 'Почтовая служба'];

function OrderPage() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {store: {data}} = useSelector((s) => s);


    const [server, setServer] = useState({
        firstName: null,
        lastName: '',
        country: [],
        phone: '',
        city: '',
        address: '',
        address2: '',
        email: '',
        deliveryType: 'Почтовая служба',
        dontCallMe: false,
        comment: '',
        id: new Date().getTime(),
    });


    const handleChangeName = (e) => {
        let code = e.target.value;
        code = code.replace(/\d/gmi, '');
        setServer({...server, firstName: code});
    }

    const handleChangeLastName = (e) => {
        let code = e.target.value;
        code = code.replace(/\d/gmi, '');
        setServer({...server, lastName: code})
    }

    const handleSelectCountry = (e) => {
        setServer({...server, country: e.target.value})
    }

    const handleChangePhone = (e) => {
        let code = e.target.value;
        code = code.replace(/\D/gmi, '');
        setServer({...server, phone: code})
    }

    const handleChangeCity = (e) => {
        let code = e.target.value;
        code = code.replace(/\d/gmi, '');
        setServer({...server, city: code})
    }

    const handleChangeAddress = (e) => {
        setServer({...server, address: e.target.value})
    }

    const handleChangeAddress2 = (e) => {
        setServer({...server, address2: e.target.value})
    }

    const handleChangeEmail = (e) => {
        setServer({...server, email: e.target.value})
    }

    const handleChangeDeliveryType = (e) => {
        setServer({...server, deliveryType: e.target.value})
    }
    const handleCheckDontCallMe = () => {
        setServer({...server, dontCallMe: !server.dontCallMe})
    }
    const handleChangeComment = (e) => {
        setServer({...server, comment: e.target.value})
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        validMail();
    }

    function validMail() {
        let re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
        let myMail = server.email;
        let valid = re.test(myMail);
        if (!valid) {
            alert('Email address mail entered not correctly!')
        }
        return valid;
    }


    function addOrderStore(server) {
        dispatch(OrderFromBasket(server));
    }


    return (
        <Box className={classes.margin}>


            <FormControl className={classes.formControl} onSubmit={handleSubmit}>
<Typography variant="h5"> Укажите ваши данные для заказа: </Typography>
                <TextField
                className={classes.input}
                type="text"
                placeholder="FirstName"
                fullWidth
                name="username"
                variant="outlined"
                value={server.firstName}
                onChange={handleChangeName}
                required
                autoFocus
                />

                <TextField
                className={classes.input}
                type="text"
                placeholder="LastName"
                fullWidth
                name="username"
                variant="outlined"
                value={server.lastName}
                onChange={handleChangeLastName}
                required
                />




                <FormControl variant="outlined">
                 <InputLabel id="simple-select-label">Country</InputLabel>
                    <Select
                    className={classes.input}
                    value={server.country}
                    onChange={handleSelectCountry}
                    inputProps={{ 'aria-label': 'Without label' }}
                    >
                    {country.map(countryLabel => (
                        <MenuItem
                            key={countryLabel}
                            value={countryLabel}>
                            {countryLabel}
                        </MenuItem>
                    ))}
                   </Select>
                </FormControl>

                <TextField
                className={classes.input}
                type="text"
                placeholder="Phone"
                fullWidth
                name="username"
                variant="outlined"
                value={server.phone}
                onChange={handleChangePhone}
                required
                />

                <TextField
                className={classes.input}
                type="text"
                placeholder="City"
                fullWidth
                name="username"
                variant="outlined"
                value={server.city}
                onChange={handleChangeCity}
                required
                />

                <TextField
                className={classes.input}
                type="text"
                placeholder="Address"
                fullWidth
                name="username"
                variant="outlined"
                value={server.address}
                onChange={handleChangeAddress}
                required
                />

                <TextField
                className={classes.input}
                type="text"
                placeholder="Address2"
                fullWidth
                name="username"
                variant="outlined"
                value={server.address2}
                onChange={handleChangeAddress2}
                required
                />

                <TextField
                className={classes.input}
                type="text"
                placeholder="Email"
                fullWidth
                name="username"
                variant="outlined"
                value={server.email}
                onChange={handleChangeEmail}
                required
                />

                <FormControl component="fieldset">
                    {deliveryType.map((prod) => (
                      <FormControlLabel
                      className={classes.input}
                            control={<Radio />}
                            value={prod}
                            label={prod}
                            key={prod}
                            name='prod'
                            checked={server.deliveryType === prod}
                            onChange={handleChangeDeliveryType}/>
                  ))}
                </FormControl>

                <FormControlLabel
                className={classes.input}
                  control={
                    <Checkbox checked={server.dontCallMe}
                    onChange={handleCheckDontCallMe}
                    name="DontCallMe" />}
                    label="DontCallMe"
                    />




                <TextField
                className={classes.input}
                type="textarea"
                placeholder="Comment"
                fullWidth
                name="username"
                variant="outlined"
                value={server.comment}
                onChange={handleChangeComment}
                maxLength={'500'}
                required
                />

                <Box className={classes.groupSubmit}>
                      <Button
                        className={classes.submit}
                        value={'Отменить'}
                        component={RouterLink}
                        to={`/basket/`}>
                        Отменить
                      </Button>

                      <Button
                        className={classes.submit}
                        value={'Подтвердить'}
                        component={RouterLink}
                        to={`/step2/`}
                        onClick={() => {addOrderStore(server)}}>
                        Подтвердить
                      </Button>
                </Box>



</FormControl>
        </Box>
    );
}


export default OrderPage;
