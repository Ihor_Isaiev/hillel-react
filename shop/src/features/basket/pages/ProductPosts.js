import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
    Box,
    CardMedia,
    makeStyles
} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';
import {removeCardFromBasket} from "../action";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';



const useStyles = makeStyles((theme) => ({
    post: {
        padding: 10,
        margin: 10,
    },
    root: {
        margin: 10,
    },
    table: {
        minWidth: 700,
    },
}));


const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: '#FF8E53',
        color: 'white',
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);


function ProductPosts(props) {


    const classes = useStyles();


    const {store: {data}} = useSelector((s) => s);

    const keys = ["rating", "categories", "isInStock", "isSale", "isNew", "photo", "price", "description", "title", "createdAt", "id"];
    let counterObj = {}
    let keyForCounterObj
    data.forEach((obj) => {
        keyForCounterObj = ''
        keys.forEach((key) => {
            keyForCounterObj += String(obj[key])
        });
        if (counterObj[keyForCounterObj]) {
            counterObj[keyForCounterObj].times++
        } else {
            counterObj[keyForCounterObj] = {
                ...obj,
                times: 1
            }
        }
    })


    let newArrayOfObjects = []
    const counterObjKeys = Object.keys(counterObj)
    counterObjKeys.forEach((key) => {
        newArrayOfObjects.push(counterObj[key])
    })

    const dispatch = useDispatch();

    function onDelete(id) {
        dispatch(removeCardFromBasket(id));
    }


    return (
        <Box>
            {newArrayOfObjects.length>0
                ?             <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="center">Photo</StyledTableCell>
                                <StyledTableCell align="center">ID</StyledTableCell>
                                <StyledTableCell align="center">title</StyledTableCell>
                                <StyledTableCell align="center">price</StyledTableCell>
                                <StyledTableCell align="center">Count</StyledTableCell>
                                <StyledTableCell align="center">Total price</StyledTableCell>
                                <StyledTableCell align="center">          </StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {newArrayOfObjects.map((item) => (
                                <StyledTableRow key={item.id}>
                                    <StyledTableCell component="th" scope="item">
                                        <CardMedia
                                            component="img"
                                            alt="photo"
                                            height="100"
                                            image={item.photo}
                                            title="random_photo"
                                        />
                                    </StyledTableCell>
                                    <StyledTableCell component="th" scope="row">{item.id}</StyledTableCell>
                                    <StyledTableCell align="right">{item.title}</StyledTableCell>
                                    <StyledTableCell align="right">{item.price}</StyledTableCell>
                                    <StyledTableCell align="right">{item.times} </StyledTableCell>
                                    <StyledTableCell align="right">{item.price * item.times} $ </StyledTableCell>

                                    <StyledTableCell align="right">
                                        <DeleteIcon color="secondary" onClick={() => onDelete(item.id)} />
                                    </StyledTableCell>

                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            :null}


        </Box>
    );
}

export default ProductPosts;
