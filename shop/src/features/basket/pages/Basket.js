import React from 'react';
import ProductPosts from "./ProductPosts";
import {Box, Button, makeStyles} from "@material-ui/core";
import { Link as RouterLink } from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import {useSelector} from "react-redux";


const useStyles = makeStyles({
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        float: 'right',
        margin: '20px',
    },
    totalItem:{
      margin: '10px',
    },
    item:{
      padding: '10px',
    }
});


function Basket() {
    const {store: {data}} = useSelector((s) => s);

    const classes = useStyles();

    let cost = (data.length < 1
        ? 0
        : data.map((item) => item.price).reduce((a, b) => +a + (+b)))

    return (
        <Box>
            <ProductPosts/>
            <Box>
                <Button className={classes.button} component={RouterLink} to={`/orders/`}>
                        Cделать заказ
                </Button>
            </Box>

            <Box className={classes.totalItem}>

                <Typography
                    className={classes.item}
                    variant="h6">
                    Всего: {data.length} продуктов в корзине
                </Typography>

                <Typography
                    className={classes.item}
                    variant="h6">
                    Сумма товаров в корзине: {cost}$
                </Typography>

            </Box>

        </Box>
    );
}

export default Basket;
