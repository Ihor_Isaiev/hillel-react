import React from 'react';
import {
  makeStyles,
   Button,
   Typography,
   Box,
 } from "@material-ui/core";
import { Link as RouterLink } from 'react-router-dom';
import {useSelector} from "react-redux";



const useStyles = makeStyles({
    button: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: '20px',
    },
    userData:{
      margin: '30px',
      backgroundColor: 'white'
    },
    text:{
      margin: '20px',
    }
});

function OrderPagesStepthree(props) {
  const classes = useStyles();

    const {store: {orders}} = useSelector((s) => s);

    return (
        <Box className={classes.userData}>
            <Typography variant={'h5'} className={classes.text}>Ваш заказ #{orders.id} оформлен. Наш менеджер с
                вами свяжется в ближайшее время.</Typography>

                <Button className={classes.button} component={RouterLink} to={`/catalog/`}>
                        Catalog
                </Button>

        </Box>
    );
}

export default OrderPagesStepthree;
