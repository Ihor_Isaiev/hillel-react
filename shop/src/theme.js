import {createMuiTheme} from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import orange from '@material-ui/core/colors/orange';


export const theme = createMuiTheme({
    spacing: 2,
    palette: {
        primary: {
            main: "#FE6B8B",
        },
        secondary: {
            main: orange[500]
        }

    }

});
